﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.txtEmptyBox = New System.Windows.Forms.TextBox()
        Me.btnUselessButton = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'txtEmptyBox
        '
        Me.txtEmptyBox.Location = New System.Drawing.Point(52, 86)
        Me.txtEmptyBox.Multiline = True
        Me.txtEmptyBox.Name = "txtEmptyBox"
        Me.txtEmptyBox.Size = New System.Drawing.Size(150, 50)
        Me.txtEmptyBox.TabIndex = 0
        Me.txtEmptyBox.Text = "This Box Does Nothing"
        '
        'btnUselessButton
        '
        Me.btnUselessButton.Location = New System.Drawing.Point(208, 86)
        Me.btnUselessButton.Name = "btnUselessButton"
        Me.btnUselessButton.Size = New System.Drawing.Size(150, 50)
        Me.btnUselessButton.TabIndex = 1
        Me.btnUselessButton.Text = "This button also does nothing"
        Me.btnUselessButton.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ActiveCaption
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnUselessButton)
        Me.Controls.Add(Me.txtEmptyBox)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents txtEmptyBox As TextBox
    Friend WithEvents btnUselessButton As Button
End Class
